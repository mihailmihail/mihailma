
Sivun urli: http://users.metropolia.fi/~mihailma/ru/html/

Tunniksilla admin/admin pääsee sisään, jos ette halua luoda omia tunnuksia.

Kirjaudu sisään -> valitse kysymys -> vastaa ja pidä hauskaa.

REST komentoja voi testata postmanilla ja tekemällämme crud_test sivustolla: 
http://users.metropolia.fi/~mihailma/ru/html/crud_test

Suosittelemme käymään edellä mainitulla sivustolla.

REST komennot löytyvät myös edellä mainitulta sivulta.

KÄYTTÄJÄTIETOKANNAN REST KUTSUT:

Käyttäjä JSON on esim. muotoa: {"ID":1,"username":"Postimies","password":"postman","email":"post@postmail.com"}

http://users.metropolia.fi/~mihailma/ru/php/?/users/allUsers/

    =>  Palauttaa kaikki käyttäjät.

http://users.metropolia.fi/~mihailma/ru/php/?/users/getUser/1/

    =>  Palauttaa käyttäjän id:llä 1.

POST postmanilla:

http://users.metropolia.fi/~mihailma/ru/php/?/users/addUser/={"username":"Postimies","password":"postman","email":"post@postmail.com"}

    => Luo uuden käyttäjän annetuilla tiedoilla. ID:llä on auto increment.

http://users.metropolia.fi/~mihailma/ru/php/?/users/delUser/={"username":"Postimies","password":"postman","email":"post@postmail.com"}

    => Poistaa kyseisen käyttäjän.


QUIZ TIETOKANTAKUTSUT:

Quiz JSON on todella iso, mutta siitä löytyy esimerkki reposta: /bin/json/subQuizQueryResultJSON.json

http://users.metropolia.fi/~mihailma/ru/php/?/quiz/allQuizzes/

    => Palauttaa kaikki quizzit.

http://users.metropolia.fi/~mihailma/ru/php/?/quiz/getQuiz/74

    => Palauttaa quizzin id:llä 74.

http://users.metropolia.fi/~mihailma/ru/php/?/quiz/getQuiz/74/getQuestions/

    => Palauttaa quiz 74:n kaikki kysymykset.

http://users.metropolia.fi/~mihailma/ru/php/?/quiz/getQuiz/74/getResults/

    => Palauttaa quiz 74:n kaikki tulosvaihtoehdot.


Kyselyiden tekemiseen on tehty html formia käyttävä työkalu Quizmaker.

Quizmaker löytyy osoitteesta:
http://users.metropolia.fi/~mihailma/ru/_dev/QuizMaker/quizmaker.html
Quizmakerillä voi syöttää kyselyitä lomakkeeseen ja sivu syöttää tiedot tietokantaan.


Jasmine unit tests

Sisältää yksikkö testit, jotka tarkistavat seuraavaa:
    1. Tietokantaan saadaan yhteys sekä sieltä voi kysellä kaikki kyselytiedot
    2. Tietokannasta tuleva data vastaa esimerkki tulostusta, joka sisältää Subway-kyselyn
    3. Tietokantaan ei voi 'kirjautua sisään' ilman käyttäjätietoja
    4. Tietokantaan ei voi 'kirjautua sisään' väärillä tiedoilla, joille ei löydy vastaavaa paria tietokannasta
    5. Tietokantaan voi 'kirjautua sisään' oikein syötetyillä tiedoilla, kuten 'Admin'-tunnuksella

http://users.metropolia.fi/~mihailma/ru/jasmine-standalone-3.3.0/SpecRunner
