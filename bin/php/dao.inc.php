<?php

class dao {

    private $db = "kallemus.";

    // User tables
    private $usersTable = "kallemus.users";
    
    // Quiz tables
    private $quizTable = "kallemus.quiz";    
    private $questionsTable = "kallemus.questions";
    private $answersTable = "kallemus.answers";
    private $resultsTable = "kallemus.results";    

    private $database = "kallemus";
    private $servername = "mysql.metropolia.fi";
    private $username = "kallemus";
    private $dbpassword = "kalle1";

    public function __construct() {

    }
    
    function createConnection() {
    
        // Create connection
        $conn = new mysqli($this->servername, $this->username, $this->dbpassword);
    
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        else {
            return $conn;
        }
    }
    
    // Close connection
    function closeConnection($conn) {
        mysqli_close($conn);
    }
    
    // Select all from User table
    function selectAll() {
    
        // Create connection
        $conn = $this->createConnection();

        // Select all from table with prepared statement.
        $stmt = $conn->prepare("SELECT * FROM " . $this->usersTable);
        $stmt->execute();
        $result = $stmt->get_result();

        // Close connection.
        $this->closeConnection($conn);
        
        // Create userDB object with users array that houses all the users in the db.
        $userDB = new stdClass();
        $i = 0;
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                $user = new stdClass();
                $user->ID = $row["ID"];
                $user->username = $row["username"];
                $user->password = $row["password"];
                $user->email = $row["email"];
                $userDB->users[$i] = $user;
                $i++;
            }
        }
        else {
            return "0 results";
        }
        // Return userDB object.
        return $userDB;
    }
    
    // Create a new user if it doesn't already exist.
    function insertUser($newUser) {
    
        if (!$this->findUsername($newUser)) {
            // Create connection
            $conn = $this->createConnection();

            if ($newUser->email === null) {
                $newUser->email = "noEmail";
            }
            // Insert new user with prepared statement.
            $stmt = $conn->prepare("INSERT INTO " . $this->usersTable . " (username, password, email) VALUES (?, ?, ?)");
            $stmt->bind_param("sss", $newUser->username, $newUser->password, $newUser->email);  
            $stmt->execute();
        
            // Close connection
            $this->closeConnection($conn);

            return "User: " . $newUser->username . " created.";
        }
        return "ERROR! User: " . $newUser->username . " already exists.";
    }

    // Delete user if it exists.
    function deleteUser($user) {
    
        if ($this->findUsername($user)) {

            // Create connection
            $conn = $this->createConnection();
        
            // Delete user based on the username.
            $stmt = $conn->prepare("DELETE FROM " . $this->usersTable . " WHERE username = ?");
            $stmt->bind_param("s", $user->username);
            $stmt->execute();
        
            // Close connection
            $this->closeConnection($conn);

            return "User: " . $user->username . " deleted.";
        }
        return "ERROR! User: " . $user->username . " not in the database.";
    }

    // Returns user info by id
    function getUser($id) {

        // Create connection
        $conn = $this->createConnection();

        // Prepared statement get the id of an user.
        $stmt = $conn->prepare("SELECT * FROM " . $this->usersTable . " WHERE ID = ?");
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        // Close connection
        $this->closeConnection($conn);

        $user = new stdClass();
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                $user->ID = $row["ID"];
                $user->username = $row["username"];
                $user->password = $row["password"];
                $user->email = $row["email"];
            }
        }
        else {
            return "0 results";
        }
        // Return user object.
        return $user;
    }

    // Returns true if the user exists
    function findUsername($user) {

        // Create connection
        $conn = $this->createConnection();

        // Prepared statement get the id of an user.
        $stmt = $conn->prepare("SELECT * FROM " . $this->usersTable . " WHERE username = ?");
        $stmt->bind_param("s", $user->username);
        $stmt->execute();
        $result = $stmt->get_result();
        
        // Close connection
        $this->closeConnection($conn);

        if (mysqli_num_rows($result) > 0) {
            return true; // Returns true if the user exists.
        }
        return false; // Returns false if the user doesn't exist.
    }
    
    // Return the number of rows in the usersTable
    function countRows($table) {
        $conn = $this->createConnection();
        $stmt = $conn->prepare("SELECT * FROM " . $table);
        $stmt->execute();
        $result = $stmt->get_result();
        $rows = mysqli_num_rows($result);
        $this->closeConnection($conn);
        return $rows;
    }

    // Check username password pair.
    function checkCredentials($user) {
    
        // Create connection
        $conn = $this->createConnection();

        // Select all from table with prepared statement.
        $stmt = $conn->prepare("SELECT * FROM " . $this->usersTable . " WHERE username = ? AND password = ?");
        $stmt->bind_param("ss", $user->username, $user->password);
        $stmt->execute();
        $result = $stmt->get_result();
    
        // Close connection.
        $this->closeConnection($conn);

        if (mysqli_num_rows($result) > 0) {
            return true; // Returns true if the username and password combination is correct.
        }
        return false;
    }

    // Get all quizzes
    function getAllQuizzes() {
        
        // Create connection
        $conn = $this->createConnection();
        mysqli_set_charset($conn, "utf8");
        
        // Get quizzes
        $stmt = $conn->prepare("SELECT QuizID FROM " . $this->quizTable);
        $stmt->execute();
        $res = $stmt->get_result();

        $quizDB = new stdClass();
        
        $i = 0;
        if (mysqli_num_rows($res) > 0) {
            while($row = mysqli_fetch_assoc($res)) {
                $quizDB->quizzes[$i] = $this->getQuiz($row["QuizID"]);
                $i++;
            }
        }
        $this->closeConnection($conn);

        return $quizDB;
    }

    // Get a specific quiz
    function getQuiz($Quiz_ID) {

        // Create connection
        $conn = $this->createConnection();
        mysqli_set_charset($conn, "utf8");

        // Get quiz
        $stmt = $conn->prepare("SELECT * FROM " . $this->quizTable . " WHERE QuizID = ?");
        $stmt->bind_param("s", $Quiz_ID);
        $stmt->execute();
        $res = $stmt->get_result();

        $quiz = new stdClass();
        $i = 0;
        if (mysqli_num_rows($res) > 0) {
            while($row = mysqli_fetch_assoc($res)) {
                $quiz->QuizID = $row["QuizID"];
                $quiz->Title = $row["Title"];
                $quiz->Description = $row["Description"];
                $quiz->QuestionCount = $row["QuestionCount"];
                $quiz->ResultCount = $row["ResultCount"];
                $quiz->QuizImageURL = $row["QuizImageURL"]; 
                $quiz->Questions = $this->getQuestions($Quiz_ID);
                $quiz->Results = $this->getResults($Quiz_ID);
            }
        }
        $this->closeConnection($conn);
        return $quiz;
    }

    // Get questions
    function getQuestions($Quiz_ID) {

        // Create connection
        $conn = $this->createConnection();
        mysqli_set_charset($conn, "utf8");

        $stmt = $conn->prepare("SELECT * FROM " . $this->questionsTable . " WHERE QuizID = ?");
        $stmt->bind_param("s", $Quiz_ID);
        $stmt->execute();
        $res = $stmt->get_result();
        
        $questions = [];
        $i = 0;
        if (mysqli_num_rows($res) > 0) {
            while($row = mysqli_fetch_assoc($res)) {
                $question = new stdClass();
                $question->QuestionID = $row["QuestionID"];
                $question->QuestionText = $row["QuestionText"];
                $question->AnswerCount = $row["AnswerCount"];
                $question->QuestionImageURL = $row["QuestionImageURL"];
                $question->QuizID = $row["QuizID"];
                $question->Answers = $this->getAnswers($question->QuestionID);
                $questions[$i] = $question;
                $i++;       
            }
        }
        $this->closeConnection($conn);
        return $questions;
    }

    // Get answers
    function getAnswers($Question_ID) {

        // Create connection
        $conn = $this->createConnection();
        mysqli_set_charset($conn, "utf8");

        $stmt = $conn->prepare("SELECT * FROM " . $this->answersTable . " WHERE QuestionID = ?");
        $stmt->bind_param("s", $Question_ID);
        $stmt->execute();
        $res = $stmt->get_result();

        $answers = [];
        $i = 0;
        while($row = mysqli_fetch_assoc($res)) {
            $answer = new stdClass();
            $answer->AnswerID = $row["AnswerID"];
            $answer->AnswerText = $row["AnswerText"];
            $answer->ResultChange = $row["ResultChange"];
            $answer->AnswerImageURL = $row["AnswerImageURL"];
            $answer->QuestionID = $row["QuestionID"];
            $answers[$i] = $answer;
            $i++;
        }
        $this->closeConnection($conn);
        return $answers;
    }

    // Get results
    function getResults($Quiz_ID) {

        // Create connection
        $conn = $this->createConnection();
        mysqli_set_charset($conn, "utf8");

        // Get results
        $stmt = $conn->prepare("SELECT * FROM " . $this->resultsTable . " WHERE QuizID = ?");
        $stmt->bind_param("s", $Quiz_ID);
        $stmt->execute();
        $res = $stmt->get_result();

        $results = [];
        $i = 0;
        if (mysqli_num_rows($res) > 0) {
            while($row = mysqli_fetch_assoc($res)) {
                $result = new stdClass();
                $result->ResultID = $row["ResultID"];
                $result->ResultName = $row["ResultName"];
                $result->ResultText = $row["ResultText"];
                $result->ResultWeightNumber = $row["ResultWeightNumber"];
                $result->ResultImageURL = $row["ResultImageURL"];
                $result->QuizID = $row["QuizID"];
                $results[$i] = $result;
                $i++;
            }
        }
        $this->closeConnection($conn);
        return $results;
    }
}

?>