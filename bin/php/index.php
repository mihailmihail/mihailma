<?php
include("dao.inc.php");


// REST stuff

// Returns numerically indexed array of URI parts.
// Example:
// resource[0] = "users"
// resource[1] = "allUsers"
function getResource() {
    $resource_string = strstr($_SERVER["REQUEST_URI"], '?');
    $resource = explode('/', $resource_string);
    array_shift($resource);
    return $resource;
}

// Returns GET, POST, etc.
function getMethod() {
    # returns a string containing the HTTP method
    $method = $_SERVER['REQUEST_METHOD'];
    return $method;
}

//................................................................

// Main

$strURL = $_SERVER{"REQUEST_URI"};
$strURL = strstr($strURL, '?');
$resource = getResource();
$request_method = getMethod();

$GET = "GET";
$POST = "POST";
$users = "users";
$allUsers = "allUsers";
$addUser = "addUser";
$getUser = "getUser";
$delUser = "delUser";
$loginUser = "loginUser";

$quiz = "quiz";
$allQuizzes = "allQuizzes";
$getQuiz = "getQuiz";

$getQuestions = "getQuestions";
$getAnswers = "getAnswers";
$getResults = "getResults";

// Creation of dao object
$dao = new dao();

// User requests
if ($resource[0] === $users) {
    switch ($request_method) {
        case $GET:
            if ($resource[1] === $allUsers) {
                jsonEcho($dao->selectAll());
            }
            else if ($resource[1] === $getUser) {
                jsonEcho($dao->getUser($resource[2]));   
            }
            break;
        case $POST:
            // Get the json string from POST
            function getString() {
                header("Content-Type: application/json");
                return json_decode(stripslashes(file_get_contents("php://input")));
            }

            function getUser() {
                $user = new stdClass();
                $user->username = $_POST["username"];
                $user->password = $_POST["password"];
                return $user;
            }

            switch ($resource[1]) {
                case $addUser:
                    echo $dao->insertUser(getString());
                    break;
                case $delUser:
                    echo $dao->deleteUser(getString());
                    break;
                case $loginUser:
                    if ($dao->checkCredentials(getString())) {
                        echo "Login successful!";
                    } else {
                        echo "Incorrect username or password.";
                    }
                    break;
            }
            break;
    }
}

// Quiz requests
else if ($resource[0] === $quiz) {
    if ($resource[1] === $getQuiz) {
        if ($resource[4] !== null) {
            if (in_array($getQuestions, $resource) && $resource[3] === $getQuestions) {
                jsonEcho($dao->getQuestions($resource[2]));
            }
            else if (in_array($getResults, $resource) && $resource[3] === $getResults) {
                jsonEcho($dao->getResults($resource[2]));
            }
        }
        else {
            jsonEcho($dao->getQuiz($resource[2]));
        }
    }
    else if ($resource[1] === $allQuizzes) {
        jsonEcho($dao->getAllQuizzes());
    }
}

// echo json object in with scandi characters
function jsonEcho($data) {
    echo json_encode($data, JSON_UNESCAPED_UNICODE);
}

?>
