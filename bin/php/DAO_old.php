<?php

// REST stuff


// Returns numerically indexed array of URI parts.
// Example:
// resource[0] = "users"
// reosuer[1] = "allUsers"
function getResource() {
    $resource_string = strstr($_SERVER["REQUEST_URI"], '?');
    $resource = explode('/', $resource_string);
    array_shift($resource);
    return $resource;
}

// Returns GET, POST, etc.
function getMethod() {
    # returns a string containing the HTTP method
    $method = $_SERVER['REQUEST_METHOD'];
    return $method;
}

//................................................................................

// Database stuff

$usersTable = "kallemus.users";

function createConnection() {
    $servername = "mysql.metropolia.fi";
    $username = "kallemus";
    $dbpassword = "kalle1";

    // Create connection
    $conn = new mysqli($servername, $username, $dbpassword);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    else {
        return $conn;
    }
}

// Close connection
function closeConnection($conn) {
    mysqli_close($conn);
}

// Select all from User table
function selectAll() {
    $sql = "SELECT * FROM " . $GLOBALS['usersTable'];

    $conn = createConnection();

    $result = mysqli_query($conn, $sql);

    $userDB = new stdClass();
    $i = 0;
    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            $user = new stdClass();
            $user->id = $row["id"];
            $user->username = $row["username"];
            $user->password = $row["password"];
            $user->email = $row["email"];
            $userDB->users[$i] = $user;
            $i++;
        }
    }
    else {
        echo "0 results";
    }
    echo json_encode($userDB);
    closeConnection($conn);
}

// Select all from User table
function insertUser() {

    $conn = createConnection();

    // Get the json string from POST
    header("Content-Type: application/json");
    $newUser = json_decode(stripslashes(file_get_contents("php://input")));

    // New user
    $id = countRows() + 1;  // Automatically increment the id based on the current rows in the table.
    $username = $newUser->username;
    $password = $newUser->password;
    $email = $newUser->email;

    // Insert new user
    $stmt = $conn->prepare("INSERT INTO " . $GLOBALS['usersTable'] . " (id, username, password, email) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("ssss", $id, $username, $password, $email);

    $stmt->execute();

    closeConnection($conn);
}

// Return the number of rows in the usersTable
function countRows() {
    $conn = createConnection();
    $sql = "SELECT * FROM " . $GLOBALS['usersTable'];
    $result = mysqli_query($conn, $sql);
    $rows = mysqli_num_rows($result);
    return $rows;
}

//................................................................

// Main

$strURL = $_SERVER{"REQUEST_URI"};
$strURL = strstr($strURL, '?');
$resource = getResource();
$request_method = getMethod();

$GET = "GET";
$POST = "POST";
$users = "users";
$allUsers = "allUsers";
$addUser = "addUser";

if ($resource[0] === $users) {
    if ($request_method === $GET && $resource[1] === $allUsers) {
        selectAll();
    }
    else if ($request_method === $POST && $resource[1] === $addUser) {
        insertUser();
    }
}

?>
