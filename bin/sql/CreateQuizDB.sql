-- --------------------------------------------------------
-- Host:                         
-- Server version:               10.2.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for mihailkysely
DROP DATABASE IF EXISTS `mihailmakysely`;
CREATE DATABASE IF NOT EXISTS `mihailmakysely` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mihailmakysely`;

-- Dumping structure for table mihailkysely.answers
DROP TABLE IF EXISTS `answers`;
CREATE TABLE IF NOT EXISTS `answers` (
  `AnswerID` int(11) NOT NULL AUTO_INCREMENT,
  `AnswerText` varchar(80) DEFAULT NULL,
  `ResultChange` varchar(255) DEFAULT NULL,
  `AnswerImageURL` varchar(255) DEFAULT NULL,
  `QuestionID` int(11) NOT NULL,
  PRIMARY KEY (`AnswerID`),
  KEY `QuestionID` (`QuestionID`),
  CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`QuestionID`) REFERENCES `questions` (`QuestionID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

-- Dumping data for table mihailkysely.answers: ~0 rows (approximately)
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;

-- Dumping structure for table mihailkysely.questions
DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `QuestionID` int(11) NOT NULL AUTO_INCREMENT,
  `QuestionText` varchar(80) DEFAULT NULL,
  `AnswerCount` int(11) DEFAULT NULL,
  `QuestionImageURL` varchar(255) DEFAULT NULL,
  `QuizID` int(11) NOT NULL,
  PRIMARY KEY (`QuestionID`),
  KEY `QuizID` (`QuizID`),
  CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`QuizID`) REFERENCES `quiz` (`QuizID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

-- Dumping data for table mihailkysely.questions: ~0 rows (approximately)
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;

-- Dumping structure for table mihailkysely.quiz
DROP TABLE IF EXISTS `quiz`;
CREATE TABLE IF NOT EXISTS `quiz` (
  `QuizID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(40) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `QuestionCount` int(11) DEFAULT NULL,
  `ResultCount` int(11) DEFAULT NULL,
  `QuizImageURL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`QuizID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

-- Dumping data for table mihailkysely.quiz: ~0 rows (approximately)
/*!40000 ALTER TABLE `quiz` DISABLE KEYS */;
/*!40000 ALTER TABLE `quiz` ENABLE KEYS */;


-- Dumping structure for table mihailkysely.results
DROP TABLE IF EXISTS `results`;
CREATE TABLE IF NOT EXISTS `results` (
  `ResultID` int(11) NOT NULL AUTO_INCREMENT,
  `ResultName` varchar(40) DEFAULT NULL,
  `ResultText` varchar(255) DEFAULT NULL,
  `ResultWeightNumber` int(11) DEFAULT NULL,
  `ResultImageURL` varchar(255) DEFAULT NULL,
  `QuizID` int(11) NOT NULL,
  PRIMARY KEY (`ResultID`),
  KEY `QuizID` (`QuizID`),
  CONSTRAINT `results_ibfk_1` FOREIGN KEY (`QuizID`) REFERENCES `quiz` (`QuizID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

-- Dumping data for table mihailkysely.results: ~0 rows (approximately)
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
/*!40000 ALTER TABLE `results` ENABLE KEYS */;

-- Dumping structure for table mihailkysely.user
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`ID`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mihailkysely.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
