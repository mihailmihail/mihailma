
INSERT INTO kallemus.quiz VALUES(0, "Mikä energiajuoma olet?", "Nyt saat viimein tietää, että kuinka alfa olet!", 7, 8, "energyDrink.png"); 

INSERT INTO kallemus.results VALUES ("Euroshopper", "Olet servun pelätyin.", 1, , 74); 
INSERT INTO kallemus.results VALUES ("Megaforce", "Olet servun toisiksi pelätyin.", 1, , 74); 
INSERT INTO kallemus.results VALUES ("Red Bull", "Olet trendikäs ja käyt paljon muotinäytöksissä.", 1, , 74);
INSERT INTO kallemus.results VALUES ("ED", "Olet varmaan kuullut, että haiset pahalle?", 1, , 74); 
INSERT INTO kallemus.results VALUES ("Battery", "Olet yhtä suomalainen kuin YLE:n Mannerheim.", 1, , 74); 
INSERT INTO kallemus.results VALUES ("Teho", "Olet työnarkomaani.", 1, , 74); 
INSERT INTO kallemus.results VALUES ("Monster", "Pidät halloweenista ja Lordista.", 1, , 74); 
INSERT INTO kallemus.results VALUES ("Mad Croc", "Olet hullu kuin krokotiili", 1, , 74); 

INSERT INTO kallemus.questions VALUES("Mikä on lempi eläimesi?", 4, , 74); 
INSERT INTO kallemus.answers VALUES ("Joku hirviö", "36", , 62); 
INSERT INTO kallemus.answers VALUES ("Krokotiili", "57", , 62); 
INSERT INTO kallemus.answers VALUES ("Härkä", "24", , 62); 
INSERT INTO kallemus.answers VALUES ("Lohikäärme", "01", , 62); 

INSERT INTO kallemus.questions VALUES("Kenen kanssa lähtisit treffeille?", 4, , 74); 
INSERT INTO kallemus.answers VALUES ("Naapurin", "23", , 63); 
INSERT INTO kallemus.answers VALUES ("Kaverin sukulaisen", "46", , 63); 
INSERT INTO kallemus.answers VALUES ("Opettajan", "17", , 63); 
INSERT INTO kallemus.answers VALUES ("Luuserin", "05", , 63); 

INSERT INTO kallemus.questions VALUES("Lemppari pelikonsolisi?", 4, , 74); 
INSERT INTO kallemus.answers VALUES ("PS4", "14", , 64); 
INSERT INTO kallemus.answers VALUES ("XBOXONE", "25", , 64); 
INSERT INTO kallemus.answers VALUES ("Switch", "36", , 64); 
INSERT INTO kallemus.answers VALUES ("En pidä pelikonsoleista", "07", , 64); 

INSERT INTO kallemus.questions VALUES("Mikä saa sinut syttymään?", 4, , 74); 
INSERT INTO kallemus.answers VALUES ("Romanttisuus", "36", , 65); 
INSERT INTO kallemus.answers VALUES ("Huumori", "02", , 65); 
INSERT INTO kallemus.answers VALUES ("Likaiset jutut", "15", , 65); 
INSERT INTO kallemus.answers VALUES ("Rohkeus", "47", , 65); 

INSERT INTO kallemus.questions VALUES("Miten tanssit klubilla?", 4, , 74); 
INSERT INTO kallemus.answers VALUES ("Heiluttamalla käsiä", "02", , 66); 
INSERT INTO kallemus.answers VALUES ("Heiluttamalla jalkoja", "45", , 66); 
INSERT INTO kallemus.answers VALUES ("5 promillen humalassa", "67", , 66); 
INSERT INTO kallemus.answers VALUES ("Seison nurkassa", "13", , 66); 

INSERT INTO kallemus.questions VALUES("Kännykäsi OS?", 4, , 74); 
INSERT INTO kallemus.answers VALUES ("iOS", "12", , 67); 
INSERT INTO kallemus.answers VALUES ("Android", "04", , 67); 
INSERT INTO kallemus.answers VALUES ("Windows", "57", , 67); 
INSERT INTO kallemus.answers VALUES ("Joku muu", "36", , 67);

INSERT INTO kallemus.questions VALUES("Lempi karkkisi?", 4, , 74); 
INSERT INTO kallemus.answers VALUES ("Salmiakki", "16", , 68); 
INSERT INTO kallemus.answers VALUES ("Suklaa", "23", , 68); 
INSERT INTO kallemus.answers VALUES ("Hedelmäkarkki", "45", , 68); 
INSERT INTO kallemus.answers VALUES ("Laku", "07", , 68); 