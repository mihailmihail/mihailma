INSERT INTO kallemus.quiz VALUES(0, "Mikä subway subi olet?", "Oletko koskaan ihmetelly, että mikä subi olet? Ei hätää. Me selvitämme sen.", 7, 9, "sub.png"); 

INSERT INTO kallemus.results VALUES ("American Steakhouse Melt", "Olet aito jenkki. Yhtä terveellinen kuin Big Mac.", 1, , 75); 
INSERT INTO kallemus.results VALUES ("Italian BMT", "Olet kuin lämmin tuulahdus Italian Toscanasta.", 1, , 75); 
INSERT INTO kallemus.results VALUES ("Spicy Italian", "Mamma Mia, ku olet kuuma!", 1, , 75); 
INSERT INTO kallemus.results VALUES ("Kana Teriyaki", "Olet yhtä päätön kuin päätön kana. Ryhmän selkeä lammas.", 1, , 75); 
INSERT INTO kallemus.results VALUES ("Kana Fajita", "Olet itsetietoinen ja itsevarma.", 1, , 75); 
INSERT INTO kallemus.results VALUES ("Kinkku", "Olet joulun kohokohta ja bileiden keskipiste!", 1, , 75); 
INSERT INTO kallemus.results VALUES ("Kalkkuna", "Olet aina kakkonen ja se toinen vaihtoehto.", 1, , 75);
INSERT INTO kallemus.results VALUES ("Veggie Delite", "Olet trendikäs ja kaikki tietää sen.", 1, , 75);

INSERT INTO kallemus.questions VALUES("Minne lähtisit mieluiten lomalle?", 5, "earth.png", 75); 
INSERT INTO kallemus.answers VALUES ("Yhdysvaltoihin", "06", , 69); 
INSERT INTO kallemus.answers VALUES ("Italiaan","12", , 69); 
INSERT INTO kallemus.answers VALUES ("Meksikoon", "34", , 69); 
INSERT INTO kallemus.answers VALUES ("En pidä matkailusta.", "75", , 69); 

INSERT INTO kallemus.questions VALUES("Kenen keikalle lähtisit?", 4, "microphone.png", 75); 
INSERT INTO kallemus.answers VALUES ("Taylor Swift", "74", , 70); 
INSERT INTO kallemus.answers VALUES ("David Guetta", "13", , 70); 
INSERT INTO kallemus.answers VALUES ("Nickelback", "05", , 70); 
INSERT INTO kallemus.answers VALUES ("Maroon5", "26", , 70); 

INSERT INTO kallemus.questions VALUES("Minkä eläimen liha on parasta?", 4, "meat.png", 75); 
INSERT INTO kallemus.answers VALUES ("Naudan", "02", , 71); 
INSERT INTO kallemus.answers VALUES ("Sian", "51", , 71); 
INSERT INTO kallemus.answers VALUES ("Kanan", "346", , 71); 
INSERT INTO kallemus.answers VALUES ("En syö lihaa, koska se on epäeettistä. #woke", "7", , 71); 

INSERT INTO kallemus.questions VALUES("Viikonlopun suunnitelmasi?", 5, "girl.png", 75);
INSERT INTO kallemus.answers VALUES ("Yökerhoon", "74", , 72); 
INSERT INTO kallemus.answers VALUES ("Baariin", "20", , 72); 
INSERT INTO kallemus.answers VALUES ("Kotona", "13", , 72); 
INSERT INTO kallemus.answers VALUES ("Töissä", "65", , 72); 

INSERT INTO kallemus.questions VALUES("Sinun arkipäiväsi?", 4, "calendar.png", 75); 
INSERT INTO kallemus.answers VALUES ("Maanantai", "23", , 73); 
INSERT INTO kallemus.answers VALUES ("Keskiviikko", "1", , 73); 
INSERT INTO kallemus.answers VALUES ("Perjantai", "67", , 73); 
INSERT INTO kallemus.answers VALUES ("Mikä arkipäivä? Viikonloppu!", "054", , 73); 

INSERT INTO kallemus.questions VALUES("Urheilu harrastuksesi?", 4, "runner.png", 75); 
INSERT INTO kallemus.answers VALUES ("Joukkuepeli", "12", , 74); 
INSERT INTO kallemus.answers VALUES ("Yleisurheilu", "46", , 74); 
INSERT INTO kallemus.answers VALUES ("Sali", "63", , 74); 
INSERT INTO kallemus.answers VALUES ("Urheilu?", "05", , 74); 

INSERT INTO kallemus.questions VALUES("Ruokajuomasi?", 4, "milk.png", 75); 
INSERT INTO kallemus.answers VALUES ("Kokis", "03", , 75); 
INSERT INTO kallemus.answers VALUES ("Viini", "127", , 75); 
INSERT INTO kallemus.answers VALUES ("Maito", "45", , 75); 
INSERT INTO kallemus.answers VALUES ("Vesi", "6", , 75);