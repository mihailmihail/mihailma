
// String variables
const POST = "POST";
const GET = "GET";

let db = "";
let json = "";
let response = "";

//
//Test suite helpers
//

function getQuizDB () {
    return ajaxTest(GET, "/quiz/allQuizzes/");
}

//Test ../js/index.js - Ajax() function without displaying any content
function ajaxTest (method, param, dataJSON) {
    let data = JSON.stringify(dataJSON);
    let phpUrl = "../php/index.php?";
    let xhr = new XMLHttpRequest();

    console.log (data);

    xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {

            // Receive quiz
            if (method === GET) {
                db = JSON.parse(xhr.responseText);
            }

            // Login check
            if (method === POST) {
                response = xhr.responseText;
            }
        }
    };
    xhr.open(method, phpUrl + param, false);

    switch (method) {
        case GET: xhr.send(); break;
        case POST: xhr.send(data); break;
    }
}

// read comparison JSON file to test database
function readJSONFile() {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', '../json/subQuizQueryResultJSON.json?', false); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            json = JSON.parse(xobj.responseText);
        }
    };
    xobj.send(null);
}

var test = (function() {
    return {
        init: function () {
            getQuizDB();
            readJSONFile();
        },

        getDB: function () {
            return db;
        },

        getJSON: function () {
            return json;
        },

        loginEmpty: function () {
            ajaxTest(POST, "/users/loginUser/");
            return response;
        },

        loginFail: function () {
            ajaxTest(POST, "/users/loginUser/", user = {
                username: "Ei tanaan",
                password: "Zurg"
            });
            return response;
        },

        loginSuccess: function () {
            ajaxTest(POST, "/users/loginUser/", user = {
                username: "admin",
                password: "admin"
            });
            return response;
        }
    }
}());