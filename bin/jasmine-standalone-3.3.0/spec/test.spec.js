

describe('Mihailmakysely', function () {

    beforeAll(function() {
        test.init();
    });

    describe('Connect to database and query quiz data', function() {
        it("should connect to database and return a JSON object", function () {
            var db = test.getDB();
            expect(db !== null).toBe(true);
        });
        it("should compare Database JSON object matches subQuizQueryResultJson.json", function () {
            var json = test.getJSON();
            var db = test.getDB();
            expect(matchQuizTitles(db, json)).toBe(true);
        });
    });

    describe('Connect to database and query user data', function () {
        it("should connect to database and fail login without credentials", function () {
            expect(test.loginEmpty()).toBe("Incorrect username or password.");
        });
        it("should connect to database and fail login with wrong credentials", function () {
            expect(test.loginFail()).toBe("Incorrect username or password.");
        });
        it("should connect to database and successfully login with correct credentials", function () {
            expect(test.loginSuccess()).toBe("Login successful!");
        });
    });
});

function matchQuizTitles (db, json) {
    for (let i = 0; i < db["quizzes"].length; i++) {
        if (db["quizzes"][i].Title === json["quizzes"][0].Title) {
            return true;
        }
    }
    return false;
}

