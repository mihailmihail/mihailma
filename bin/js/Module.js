let $ = function () {
    return {
        id: function (id) {
            return document.getElementById(id);
        },
        value: function (id) {
            return document.getElementById(id).value;
        },
        imgFolder : function () {
            return "../img/";
        }
    }
}();