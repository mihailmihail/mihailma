
// String variables
const show = "block";
const hide = "none";
const grid = "grid";

// Ajax string vars
const loginSuccessMessage = "Login successful!";
const POST = "POST";
const GET = "GET";

// Quiz database object.
let quizDB = "";
let currentQuiz = "";

// Result number arr
let resultCalcArr = [];

window.onbeforeunload = function() {
    return "Sivun päivittäminen kirjaa sinut ulos. Haluatko jatkaa?";
};

window.onload = function () {
    showHideBackDiv(hide);
    showHideAlertDiv(hide);
    showHideQuizzesQuestions(hide, hide);
    showHideLoginLogout(show, hide);
    let enter = "Enter";
    let loginButton = $.id("loginButton");
    let signupButton = $.id("signupButton");
    let newSignupButton = $.id("signup");
    let signupbackbutton = $.id("signupbackbutton");
    let passwordText = $.id("password");
    let usernameText = $.id("username");
    let newUsername = $.id("newUsername");
    let newUserPassText = $.id("newUserpass");
    let newUserPassAgainText = $.id("newUserpassAgain");
    let logoutButton = $.id("logoutButton");
    let cookieButton = $.id("cookieButton");
    let cookieConsent = $.id("cookie-consent");

    // log in by pressing enter on password-field
    passwordText.addEventListener("keydown", e => {
        if (e.key === enter) {
            login();
        }
    });

    // log in by pressing enter on username-field
    usernameText.addEventListener("keydown", e => {
        if (e.key === enter) {
            login();
        }
    });
    
    // log in button event
    loginButton.addEventListener("click", e => {
        login();
    });

    // Move to sign up view
    signupButton.addEventListener("click", e => {
        showHideAlertDiv(hide);
        signup();
    });

    // Send new user data to DB
    newSignupButton.addEventListener("click", e => {
        checkNewUser();
    });

    // Go back from sign up view
    signupbackbutton.addEventListener("click", e => {
        $.id("signup-wrapper").style.display = "none";
        showHideLoginLogout(show, hide);
        showHideAlertDiv(hide);
    });

    // log out button event
    logoutButton.addEventListener("click", e => {
        logout();
    });

    // back buttons event
    let backButtons = document.getElementsByClassName("back-button-wrapper");
    for (let i = 0; i < backButtons.length; i++) {
        backButtons[i].addEventListener("click", e => {
            goBack();
        })
    }

    // Close the cookie consent
    cookieButton.addEventListener("click", function () {
        cookieConsent.className = "cookie-out cookie-consent";
        setTimeout(function () {
            cookieConsent.setAttribute("style", "display: none;")
        }, 2000);
    });

    $.id("alertDiv").addEventListener("click", function () {
       showHideAlertDiv(hide);
    });

    newUsername.addEventListener("keydown", e => {
        if (e.key === enter) {
            checkNewUser();
        }
    });

    newUserPassText.addEventListener("keydown", e => {
        if (e.key === enter) {
            checkNewUser();
        }
    });

    newUserPassAgainText.addEventListener("keydown", e => {
        if (e.key === enter) {
            checkNewUser();
        }
    })
};

function checkNewUser() {
    if($.value("newUserpass") !== $.value("newUserpassAgain")){
        showHideAlertDiv(show, "Salasanat eivät täsmää!!", "red");
    } else {
        ajax(POST, "/users/addUser/", getUser($.value("newUsername"), $.value("newUserpass")));
    }
}

function getUser(username, password) {
    return {
        username:username,
        password:password
    };
}

function ajax(method, param, dataJSON) {
    let phpUrl = "../php/index.php?";
    let xhr = new XMLHttpRequest();
    let data = JSON.stringify(dataJSON);
    xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            // Receive quiz
            if (method === GET) {
                quizDB = JSON.parse(xhr.responseText);
                displayQuizzes();
            }

            // Create new user
            if (param === "/users/addUser/") {
                $.id("alertDiv").style.animation = "none";
                if (xhr.responseText === "User: " + dataJSON.username + " created." 
                    && method === POST) {
                    xhr.responseText = "";
                    showHideAlertDiv(show, "Käyttäjä luotu.", "rgba(0, 190, 255, 0.9)");
                }
                else {
                    showHideAlertDiv(show, "Käyttäjänimi on varattu.", "red");
                }
            }

            // Login check
            else if (param === "/users/loginUser/") {
                if (xhr.responseText === loginSuccessMessage && method === POST) {
                    xhr.responseText = "";
                    loginSuccessful();
                } 
                else {
                    showHideAlertDiv(show, "Väärä tunnus ja/tai salasana!", "red");
                    $.id("alertDiv").style.animation = "blinking-alert 1s infinite";
                }
            }
        }
    };
    xhr.open(method, phpUrl + param, true);
    
    switch (method) {
        case GET: xhr.send(); break;
        case POST: xhr.send(data); break;
    }
}

// Hide log in view, show sign up view
function signup() {
    showHideLoginLogout(hide, hide);
    $.id("signup-wrapper").style.display = "block";
}

// Login button pressed.
function login() {
    ajax(POST, "/users/loginUser/", getUser($.value("username"), $.value("password")));
}   

// When login is successful
function loginSuccessful() {
    showHideLoginLogout(hide, show);
    showHideAlertDiv(hide);
    getQuizzes();
}

// Logout button pressed.
function logout() {
    if (confirm("Haluatko varmasti kirjautua ulos?")) {
        showHideLoginLogout(show, hide);
        showHideAlertDiv(hide);
        showHideQuizzesQuestions(hide, hide);
        clearChildNodes("quizGrid");
        clearChildNodes("questionList");
        showHideBackDiv(hide);
    }
}

// Displaying or hiding the alert field.
function showHideAlertDiv(value, text, color) {
    $.id("alertDiv").style.display = value;
    $.id("alertDiv").innerHTML = text;
    $.id("alertDiv").style.backgroundColor = color;
}

// Displaying or hiding the back button.
function showHideBackDiv(value) {
    let arr = document.getElementsByClassName("back-button-wrapper");
    for (let i = 0; i < arr.length; i++) {
        arr[i].style.display = value;
    }
}

function scrollToTop() {
    window.scrollTo(0, 0);
}

// Back button pressed.
function goBack() {
    scrollToTop();
    showHideLoginLogout(hide, show);
    clearChildNodes("questionList");
    showHideQuizzesQuestions(grid, hide);
    showHideBackDiv(hide);
    showHideResult(hide);
}

// Clear all the child nodes from a parent.
function clearChildNodes(id) {
    let node = $.id(id);
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }
}

// Show or hide the login div or the logout div
function showHideLoginLogout(loginDiv, logoutDiv) {
    $.id("loginDiv").style.display = loginDiv;
    $.id("logoutDiv").style.display = logoutDiv;
}

// Show or hide the quiz list or the questions list.
function showHideQuizzesQuestions(quizGrid, questionList) {
    $.id("quizGrid").style.display = quizGrid;
    $.id("questionList").style.display = questionList;
}

// Get quiz database.
function getQuizzes() {
    ajax(GET, "/quiz/allQuizzes/");
}

// Display quizzes in a grid and add button click listeners.
function displayQuizzes() {
    showHideQuizzesQuestions(grid, hide);  
    let arr = quizDB.quizzes;
    for (let i = 0; i < arr.length; i++) {
        let title = document.createTextNode(arr[i].Title);
        let quizBox = document.createElement("div");
        quizBox.id = arr[i].QuizID;
        quizBox.addEventListener("click", e => {
            displayQuestions(quizBox.id);
        });
        quizBox.className = "quiz-box";
        
        // Quizbox image
        let imgUrl = arr[i].QuizImageURL;
        if (imgUrl != null) {
            quizBox.appendChild(createImage(imgUrl));
        }

        // Add title text to the quizBox.
        quizBox.appendChild(title);

        $.id("quizGrid").appendChild(quizBox);
    }
}

// Display questions in a list and create child answergrid.
function displayQuestions(quizId) {
    showHideBackDiv(show);
    showHideLoginLogout(hide, hide);
    showHideQuizzesQuestions(hide, show);

    // Array for storing every answer in the current quiz.
    // Used for checking when all the questions have been answered.
    let answerArr = [];

    // Clear resultCalcArr
    resultCalcArr = [];

    // Find current questions and quiz.
    let arr = quizDB.quizzes;
    let questions = [];
    arr.forEach(e => {
        if (e.QuizID == quizId) {
            questions = e.Questions;
            currentQuiz = e;
        }
    });

    // Load the correct questions and create the question boxes.
    for (let i = 0; i < questions.length; i++) {
        let questionText = document.createTextNode(questions[i].QuestionText);
        let questionBox = document.createElement("div");
        questionBox.appendChild(questionText);
        questionBox.className = "question-box";
        questionBox.setAttribute("id", "q" + i);
        if (i > 0) { // Hides every question except the first one
            questionBox.style.display = hide;
        }

        // QuestionBox image
        let imgUrl = questions[i].QuestionImageURL;
        if (imgUrl != null) {
            questionBox.appendChild(createImage(imgUrl));
        }

        $.id("questionList").appendChild(questionBox);
        $.id("q0").scrollIntoView({behavior: "smooth", block: "start"});
        displayAnswers(questions, i, answerArr);
    }
}

// Display answers in a grid below the question box and
// create answer boxes with button click listeners.
function displayAnswers(questions, currentQuestion, answerArr) {
    let answerGrid = document.createElement("div");

    // Answer grid houses answer boxes
    answerGrid.className = "answer-grid";
    answerGrid.setAttribute("id", "a" + currentQuestion);
    if (currentQuestion > 0) {
        answerGrid.style.display = hide;
    }
    answerGrid.aBoxList = [];

    let answers = questions[currentQuestion].Answers;
    for (let i = 0; i < answers.length; i++) {
        let answerText = document.createTextNode(answers[i].AnswerText);
        let answerBox = document.createElement("div");
        
        // AnswerBox image
        let imgUrl = answers[i].AnswerImageURL;
        answerBox.appendChild(createImage(imgUrl));

        // AnswerBox properties
        answerBox.appendChild(answerText);
        answerBox.className = "answer-box";
        answerBox.clicked = false;
        answerBox.resultChange = answers[i].ResultChange;
        
        // AnswerGrid has an array of its children (answerBoxes);
        answerGrid.aBoxList[i] = answerBox;
        answerArr.push(answerBox);
        
        answerBox.addEventListener("click", e => {
            if (!answerBox.clicked) {
                answerSelected(answerBox, answerArr);
            }
        });
        answerGrid.appendChild(answerBox);
    }
    $.id("questionList").appendChild(answerGrid);
}

// Calculate result
function calcResult() {
    let store = (resultCalcArr.join("")).split("");
    let frequency = {};  // array of frequency.
    let max = 0;  // holds the max frequency.
    let result;   // holds the max frequency element.
    for (let v in store) {
        frequency[store[v]] = (frequency[store[v]] || 0) + 1; // increment frequency.
        if(frequency[store[v]] > max) { // is this frequency > max so far ?
                max = frequency[store[v]];  // update max.
                result = store[v];          // update result.
        }
    }
    return result;
}

// When an answer is selected make the selection final and add some styling.
function answerSelected(answerBox, answerArr) {
    let arr = answerBox.parentNode.aBoxList;
    resultCalcArr.push(answerBox.resultChange);
    arr.forEach(element => {
        if (element.innerHTML !== answerBox.innerHTML) {
            element.style.backgroundColor = "#eaeaea33";
            element.style.color = "#00000077";
        } 
        else {
            answerBox.style.border = "2px solid black";
            answerBox.style.background = "#c8c8c8";
        }
        element.clicked = true;
    });

    // Checks if every question has been answered
    if (answerArr.every(isAnswerChosen)) {
        displayResult();
        return;
    }

    // Scrolls the next question to top
    let nextIndex = parseInt(answerBox.parentNode.id.replace('a','')) + 1;
    $.id("a" + nextIndex).removeAttribute("style");
    $.id("q" + nextIndex).removeAttribute("style");
    $.id("q" + nextIndex).scrollIntoView({behavior: "smooth", block: "start"});
}

// Function is in conjunction with the above answerArr.every.
function isAnswerChosen(answerBox) {
    return answerBox.clicked;
}

// Shows or hides the result field.
function showHideResult(value) {
    $.id("resultDiv").style.display = value;
    if (value === hide) {
        clearChildNodes("resultDiv"); 
    }
    let arr = document.getElementsByClassName("result-box");
    for (let i = 0; i < arr.length; i++) {
        arr[i].style.display = value;
    }
}

// Create dynamic result elements.
function displayResult() {
    showHideQuizzesQuestions(hide, hide);
    scrollToTop();
    let resultDiv = $.id("resultDiv");
    showHideResult(show);

    // Calculate correct result
    let i = calcResult();
    
    // Get result texts
    let quizTitle = document.createTextNode(currentQuiz.Title);
    let resultName = document.createTextNode(currentQuiz.Results[i].ResultName);
    let resultText = document.createTextNode(currentQuiz.Results[i].ResultText);

    // Create result boxes
    let quizTitleBox = document.createElement("div");
    let resultNameBox = document.createElement("div");
    let resultDescBox = document.createElement("div");

    // quizTitleBox Image
    let quizImgUrl = currentQuiz.QuizImageURL;
    if (quizImgUrl != null) {
        quizTitleBox.appendChild(createImage(quizImgUrl));
    }

    // resultNameBox Image
    let imgUrl = currentQuiz.Results[i].ResultImageURL;
    if (imgUrl != null) {
        resultNameBox.appendChild(createImage(imgUrl));
    }

    // Give boxes css class
    quizTitleBox.className = "result-box";
    resultNameBox.className = "result-box";
    resultDescBox.className = "result-box";

    // Give boxes css id
    resultNameBox.id = "resultNameBox";
    resultDescBox.id = "resultDescBox";

    // Add text and elements together.
    quizTitleBox.appendChild(quizTitle);
    resultNameBox.appendChild(resultName);
    resultDescBox.appendChild(resultText);
    resultDiv.appendChild(quizTitleBox);
    resultDiv.appendChild(resultNameBox);
    resultDiv.appendChild(resultDescBox);

    resultDiv.scrollIntoView({behavior: "smooth"});
}

// Create background image for various boxes.
function createImage(imgUrl, width, height) {
    let url = $.imgFolder() + imgUrl;
    let image = document.createElement("div");
    image.style.backgroundImage = "url(" + url + ")";

    // If width or height are not given
    if (width == null || height == null ) {
        width = "300px";
        height = "160px";
    }

    // Image styling 
    image.style.backgroundRepeat = "no-repeat";
    image.style.backgroundPosition = "center";
    image.style.position = "absolute";
    image.style.opacity = "0.2";
    image.style.backgroundColor = "transparent";
    image.style.width = width;
    image.style.height = height;

    return image;
}