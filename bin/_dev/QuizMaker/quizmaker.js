$(document).ready(function(){

    // Maximum allowed varchar length
    const varchar_max_length = 255;

    // Result id prefix
    const result_identifier = "result_";

    // Question id prefix
    const question_identifier = "question_";

    // Answer id prefix
    const answer_identifier = "answer_";

    /*
     * Initializes a <fieldset> from #image_fieldset template with listeners.
     * <fieldset> includes hasImg checkbox and
     * text input for the image path.
     *
     * @return Returns a initialized <fieldset> for image input with listeners.
     */
    function createImageFieldset() {

        // Load #image_fieldset template
        let fieldset = $($('#image_fieldset').html());

        // Load hasImg checkbox
        let hasImg = fieldset.children("input[type='checkbox']");

        // Load Image path text input
        let imgPath = fieldset.children("input[type='text']");

        // Change the text input property 'disabled' to !checkbox.checked when the checkbox value changes
        hasImg.change(function () {
            imgPath.attr('disabled', !this.checked);
        });

        return fieldset;
    }

    /*
     * Creates quiz <fieldset> with image <fieldset> from template #quiz_set.
     */
    function createQuiz() {

        // Load quiz template
        let quiz = $($('#quiz_set').html());
        console.log(quiz);
        // Add image <fieldset>
        quiz.append(createImageFieldset());

        // Add initialized quiz form to document
        $("#quiz").append(quiz);
    }

    /*
     * Creates result <fieldset> with image <fieldset> from template #result_form.
     * Adds id="" attribute to the result with 'result_identifier' + order number
     * Adds the result <fieldset> to #all_results.     *
     */
    function createResult() {

        // Load total number of results.
        let numOfResults = $("fieldset[class='result']").length;

        // Load #result_form template.
        let result = $($('#result_form').html());

        // Set result ID.
        result.attr('id' , result_identifier + numOfResults);

        // Load result name & Set name to result name field.
        let resultName = result.children('input[type="text"]');
        resultName.attr('name' , "result[" + numOfResults + "][" + "title" + "]");

        // Load result text area & Set name to result text area.
        let resultText = result.children('textarea[name="result_text"]');
        resultText.attr('name' , "result[" + numOfResults + "][" + "text" + "]");

        // Load characters left span & Bind text area and characters left span.
        let charLeft = result.children('span[class="text_char_left"]');
        resultText.bind('input propertychange' ,function () {
            charLeft.text(varchar_max_length - $(this).val().length);
        });

        // Load result weight number input range & Set range name.
        let resultWeightNumber = result.children('input[name="weight_number"]');
        resultWeightNumber.attr('name', "result[" + numOfResults + "][" + "resultweight" + "]");

        // Load result weight number display & Update the weight number result display.
        let resultWeightText = result.children('span[class="result_weight_text"]');
        resultWeightNumber.change(function () {
            resultWeightText.text(resultWeightNumber.val())
        });

        // Add image <fieldset>
        let imageField = createImageFieldset();

        // Name hasImg checkbox
        let hasImg = imageField.children("input[type='checkbox']");
        hasImg.attr('name' , "result[" + numOfResults + "][" + "has_image" + "]");

        // Name Img path field
        let imgPath = imageField.children("input[type='text']");
        imgPath.attr('name' , "result[" + numOfResults + "][" + "img_path" + "]");

        result.append(imageField);

        // Add initialized result form to document
        $("#all_results").append(result);
    }

    /*Question*/
    /*
     * Creates question <fieldset> with imgge <fieldset> from template #question_form.
     * Adds id="" attribute to the result with 'question_identifier' + order number.
     * Adds the result to the document.     *
     */
    function createQuestion() {

        /*
         * Creates an answer <fieldset> with image <fieldset> from template #image_fieldset.
         *
         * @return initialized answer form ready to use.
         */
        function createAnswer() {
            // Load total number of answers.
            // BUG :: Looks for all the answers in all questions
            // SOLUTION :: Narrow the search for only this question
            let numOfAnswers = $("fieldset[class='answer']").length;

            // Load the #answer_form template.
            let answer = $($('#answer_form').html());

            // Set the answer ID.
            answer.attr('id' , answer_identifier + numOfAnswers);

            // Load answer text area & characters left.
            let answerText = answer.children('textarea[name="answer_text"]');
            let charLeft = answer.children('span[class="text_char_left"]');

            // Name answer text area.
            answerText.attr('name' , "question[" + numOfQuestions + "][" + "answer" + "][" + numOfAnswers + "][" + "text" + "]");

            // Bind text area and characters left span.
            answerText.bind('input propertychange' ,function () {
                charLeft.text(varchar_max_length - $(this).val().length);
            });

            // Name result weights
            let resultChange = answer.find("input[type='checkbox']");
            for(let i = 0; i < resultChange.length; i++){
                resultChange[i].name = "question[" + numOfQuestions + "]" +
                    "[" + "answer" + "][" + numOfAnswers + "]" +
                    "[" + "resultWeight" + "][" + i + "]";
            }

            // Add image <fieldset>
            let imageField = createImageFieldset();

            // Load & Name hasImg
            let hasImg = imageField.children("input[type='checkbox']");
            hasImg.attr('name' , "question[" + numOfQuestions + "][" + "answer" + "][" + numOfAnswers + "][" + "has_image" + "]");

            // Load & Name Img path text input
            let imgPath = imageField.children("input[type='text']");
            imgPath.attr('name' , "question[" + numOfQuestions + "][" + "answer" + "][" + numOfAnswers + "][" + "img_path" + "]");

            answer.append(imageField);

            return answer;
        }

        // Total number of questions
        let numOfQuestions = $("fieldset[class='question']").length;

        // Load #question_form template
        let question = $($('#question_form').html());

        // Set question ID.
        question.attr('id' , question_identifier + numOfQuestions);

        // Load answer text area & characters left.
        let questionText = question.children('textarea[name="question_text"]');
        let charLeft = question.children('span[class="text_char_left"]');

        // Name question text
        questionText.attr('name' , "question[" + numOfQuestions + "][" + "text" + "]");

        // Bind text area and characters left span.
        questionText.bind('input propertychange' ,function () {
            charLeft.text(varchar_max_length - $(this).val().length);
        });

        let imageField = createImageFieldset();

        // Name hasImg
        let hasImg = imageField.children("input[type='checkbox']");
        hasImg.attr('name' , "question[" + numOfQuestions + "][" + "has_image" + "]");

        // Name Img path field
        let imgPath = imageField.children("input[type='text']");
        imgPath.attr('name' , "question[" + numOfQuestions + "][" + "img_path" + "]");

        // Add image <fieldset>
        question.append(imageField);

        // Add initialized question form to document.
        $("#all_questions").append(question);

        // Configure add new answer button
        question.children('button[name="new_answer"]').on('click', function () {
            question.children('fieldset[name="all_answers"]').append(createAnswer());
        });
    }


    // Create quiz <fieldset>
    createQuiz();

    // Configure document buttons.
    $("#new_result").on('click', createResult);
    $("#new_question").on('click', createQuestion);

});