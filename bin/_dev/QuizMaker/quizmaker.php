<?php
/**
 * Created by PhpStorm.
 * User: setup
 * Date: 28.11.2018
 * Time: 11.38
 */
include("dbconn.php");

$quiz = new stdClass();
$database = new dbconn();
$results = [];
$questions = [];
$inserts = [];

// Fetch and create new result objects from the form. Objects are then passed
// to the corresponding array.
if(isset($_POST["result"])){
    foreach ($_POST["result"] as $result){
        $object = new stdClass();
        $object -> id = 0;
        $object -> title = $result['title'];
        $object -> text = $result['text'];
        $object -> weightNumber = 1;
        $object -> hasImg = (isset($result["has_image"]) ? true : false);
        $object -> imgPath = (isset($result["img_path"]) ? $result["img_path"] : null);

        array_push($results,$object);
    }
}

// Fetch and create new question objects from the form. Objects are then passed
// to the corresponding array.
if(isset($_POST["question"])){
    foreach ($_POST["question"] as $question){
        $object = new stdClass();
        $object -> text = $question['text'];
        $object -> hasImg = (isset($question["has_image"]) ? true : false);
        $object -> imgPath = (isset($question["img_path"]) ? $question["img_path"] : null);
        $object -> answers = [];
        $object -> answerCount = 0;

        // Fetch and create new answer objects from the form. Objects are then passed
        // to the question object answers array.
        if(isset($question["answer"])){
            foreach ($question["answer"] as $answer){
                $answerObject = new stdClass();
                $answerObject -> text = $answer["text"];
                $answerObject -> hasImg = (isset($answer["has_image"]) ? true : false);
                $answerObject -> imgPath = (isset($answer["img_path"]) ? $answer["img_path"] : null);
                $answerObject -> resultChange = [];

                foreach ($answer["resultWeight"] as $i=>$change){
                    array_push($answerObject->resultChange, $i);
                }

                array_push($object->answers, $answerObject);
                $object->answerCount++;
            }
        }

        array_push($questions,$object);
    }
}

// Quiz information
$quiz -> title = $_POST["quiz_title"];
$quiz -> description = $_POST["quiz_desc"];
$quiz -> hasImg = (isset($_POST["has_image"]) ? true : false);
$quiz -> imgPath = (isset($_POST["img_path"]) ? $_POST["img_path"] : null);
$quiz -> id = 0;
$quiz -> questionCount = sizeof($questions);
$quiz -> resultCount = sizeof($results);

// Insert to Database
$database->insertQuiz($quiz);
$database->insertResults($results, $quiz->id);
$database->insertQuestions($questions, $quiz->id);

// Return the insert statement to the user.
echo $database->readQuery();
?>