<?php
/**
 * Created by PhpStorm.
 * User: Matias
 * Date: 18/11/29
 * Time: 9:44
 */

class dbconn
{
    // Server info
    private $servername = "mysql.metropolia.fi";
    private $username = "kallemus";
    private $dbpassword = "kalle1";
    private $query = "";

    // Constructor
    public function __construct() {

    }

    /* Establishes a database connection and returns it.
     * @return Object with active database connection */
    function createConnection() {

        // Create connection
        $conn = new mysqli($this->servername, $this->username, $this->dbpassword);

        // Set charset
        mysqli_set_charset($conn, "utf8");

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        else {
            return $conn;
        }
    }

    // Closes the inserted database connection
    function closeConnection($conn) {
        mysqli_close($conn);
    }

    /* Fetches and returns the Quiz ID with the given title
     * @param $title Title of the quiz
     * @return ID
     */
    function getQuizID($title){
        // Table prefill
        $quizTable = "kallemus.quiz";

        // Connection
        $conn = $this->createConnection();

        // Prepare the connection
        $stmt = $conn->prepare("SELECT QUIZID FROM $quizTable WHERE TITLE= ?");

        // Bind parameters
        $stmt->bind_param("s", $title);

        // Execute
        $stmt->execute();

        // Fetch and parse results
        $result = $stmt->get_result()->fetch_row()[0];

        // Close connection
        $this->closeConnection($conn);
        return $result;
    }

    /* Insert a new quiz to Kallemu.quiz table
     * @param $quiz Quiz object
     */
    function insertQuiz($quiz) {

        // Table prefill
        $quizTable = "kallemus.quiz";

        // Open connection
        $conn = $this->createConnection();

        // Prepare statement
        $stmt_quiz = $conn->prepare("INSERT INTO $quizTable VALUES(?, ?, ?, ?, ?, ?)");

        // Store the whole query
        $this->query .= "INSERT INTO $quizTable VALUES($quiz->id, $quiz->title, $quiz->description, $quiz->questionCount, $quiz->resultCount, $quiz->imgPath); ";

        // Bind parameters
        $stmt_quiz->bind_param("issiis", $quiz->id, $quiz->title, $quiz->description, $quiz->questionCount, $quiz->resultCount, $quiz->imgPath);

        // Execute
        $stmt_quiz->execute();

        // Close connection
        $this->closeConnection($conn);

        // Update the right ID to quiz object
        $quiz->id = $this->getQuizID($quiz->title);
    }

    /* Insert all results to kallemus.results
     * @ param $results array of Result object
     * @ param $quizid ID of the quiz which the results belong to
     */
    function insertResults($results, $quizid){

        // Table prefill
        $resultTable = "kallemus.results";

        // Cycle through all the results and feed them to database
        foreach($results as $result){
            $conn = $this->createConnection();
            $stmt_result = $conn->prepare("INSERT INTO $resultTable VALUES(0, ?, ?, ?, ?, ?)");
            $this->query .= "INSERT INTO $resultTable VALUES ($result->title, $result->text, $result->weightNumber, $result->imgPath, $quizid); ";
            $stmt_result->bind_param("ssisi", $result->title, $result->text, $result->weightNumber, $result->imgPath, $quizid);
            $stmt_result->execute();
            $this->closeConnection($conn);
        }
    }

    /* Get ID of the question which text matches to the text given
     * @param $text Question text to be searched from the databass
     * @return result ID
     */
    function getQuestionID($text) {
        $quizTable = "kallemus.questions";
        $conn = $this->createConnection();

        $stmt = $conn->prepare("SELECT QuestionID FROM $quizTable WHERE QuestionText= ?");

        $stmt->bind_param("s", $text);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_row()[0];

        $this->closeConnection($conn);
        return $result;
    }

    /* Insert all questions to kallemus.questions
     * @param $questions Array of Question objects
     * @param $quizid QuizID which the questions belong to
     */

    function insertQuestions($questions, $quizid){

        // Table prefill
        $questionTable = "kallemus.questions";
        $answerTable = "kallemus.answers";

        // Connection
        $conn = $this->createConnection();

        // Cycle through the question array and feed them to database.
        foreach ($questions as $question){
            $stmt_question = $conn->prepare("INSERT INTO $questionTable VALUES(0,?,?,?,?)");
            $this->query .= "INSERT INTO $questionTable VALUES($question->text, $question->answerCount, $question->imgPath, $quizid); ";
            $stmt_question->bind_param("sisi", $question->text, $question->answerCount, $question->imgPath, $quizid);
            $stmt_question->execute();
            $question->id = $this->getQuestionID($question->text);

            // Cycle through answers and feed them to database.
            foreach ($question->answers as $answer){
                $stmt_answer = $conn->prepare("INSERT INTO $answerTable VALUES(0,?,?,?,?)");
                $this->query .= "INSERT INTO $answerTable VALUES ($answer->text," . implode($answer->resultChange) . ", $answer->imgPath, $question->id); ";
                $stmt_answer->bind_param("sssi", $answer->text, implode($answer->resultChange), $answer->imgPath, $question->id);
                $stmt_answer->execute();
            }
        }

        // Close connection
        $this->closeConnection($conn);
    }
}